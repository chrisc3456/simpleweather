package com.simple.weather.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.simple.weather.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
